import firebase from 'firebase/app'
import 'firebase/auth'

const firebaseConfig = {
    apiKey: "AIzaSyClrFzUcWvHqKkmXfNd8Weo9CX4U3NpagY",
    authDomain: "bootcamp-d2830.firebaseapp.com",
    projectId: "bootcamp-d2830",
    storageBucket: "bootcamp-d2830.appspot.com",
    messagingSenderId: "169414957810",
    appId: "1:169414957810:web:f001c8233ebb75867b0e6b"
  };
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const auth = firebase.auth()
const provider = new firebase.auth.GoogleAuthProvider()

firebase.getCurrentUser = () => {
    return new Promise((resolve, reject) => {
        const unsubscribe = firebase.auth().onAuthStateChanged(user => {
            unsubscribe()
            resolve(user)
        }, reject) 
    })
}

export {auth, provider, firebase}