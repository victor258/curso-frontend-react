import {useDispatch} from 'react-redux'
import {accionLogout} from '../redux/userReducer'
import {useHistory} from 'react-router-dom'

const Navbar = () => {

    const dispatch = useDispatch()
    let history = useHistory()

    const cerrar = () => {
        dispatch(accionLogout())
        history.push('/login')
    }

    return (
        <nav>
            <button onClick={cerrar}>Cerrar Sesión</button>
        </nav>
    )
}

export default Navbar
