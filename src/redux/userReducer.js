import {provider, auth, firebase} from '../firebase'

const dataInicial = {
    loading: false,
    activo: false
}

const USER_EXITO = 'USER_EXITO'
const LOADING = 'LOADING'
const USER_ERROR = 'USER_ERROR'
const USER_LOGOUT = 'USER_LOGOUT'

export default function userReducer (state = dataInicial, action) {
    switch(action.type){
        case LOADING:
            return {...state, loading: true}
        case USER_EXITO:
            return {...state, loading: false, activo: true, user: action.payload.user}
        case USER_ERROR:
            return {...dataInicial}
        case USER_LOGOUT:
            return {...dataInicial}
        default:
            return {...dataInicial}
    }
}

export const accionLogout = () => dispatch => {
    auth.signOut()
    dispatch({
        type: USER_LOGOUT 
    })
}

export const accionLogin = () => async(dispatch) => {
    dispatch({
        type: LOADING
    })

    try {
        const res = await auth.signInWithPopup(provider)
        console.log(res)
        dispatch({
            type: USER_EXITO,
            payload: {
                user: {
                    uid: res.user.uid,
                    email: res.user.email,
                }
            }
        })
    } catch (error) {
        console.log(error)
        dispatch({type: USER_ERROR})
    }
}

export const accionGetCurrentUser = () => async (dispatch) => {
    dispatch({
        type: LOADING
    })
    try {
        const res = await firebase.getCurrentUser()
        if(!res){
            throw new Error('Sin user')
        }
        dispatch({
            type: USER_EXITO,
            payload: {
                user: {
                    uid: res.uid,
                    email: res.email,
                }
            }
        })
    } catch (error) {
        console.log(error)
        dispatch({type: USER_ERROR})
    }
}


