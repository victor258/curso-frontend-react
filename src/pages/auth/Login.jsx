import {useEffect} from 'react'

import {useDispatch, useSelector} from 'react-redux'
import {accionLogin} from '../../redux/userReducer'

import {withRouter} from 'react-router-dom'

const Login = (props) => {

    const dispatch = useDispatch()
    const loading = useSelector(store => store.user.loading)
    const activo = useSelector(store => store.user.activo)

    useEffect(() => {
        console.log('activo', activo)
        if(activo){
            props.history.push('/')
        }
    },)

    return (
        <div>
           <h1>Login</h1> 
           <button onClick={() => dispatch(accionLogin())} disabled={loading}>Acceder</button>
        </div>
    )
}

export default withRouter(Login)
