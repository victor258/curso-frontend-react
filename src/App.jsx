import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import { useSelector } from "react-redux";

import Login from "./pages/auth/Login";
import Inicio from "./pages/Inicio";

import Navbar from "./components/Navbar";


const App = () => {
  const loading = useSelector(store => store.user.loading)
  const activo = useSelector(store => store.user.activo)
  
  const RutaProtegida = ({component, path, ...rest}) => {
    if(activo){
      return <Route component={component} path={path} {...rest} />
    }else {
      return <Redirect to="/login" {...rest} />
    }
  }

  if(loading){
    return <p>Loading...</p>
  }

  return (
    <Router>
      <Navbar />
      <Switch>
        <RutaProtegida path="/" exact component={Inicio} />
        <Route path="/login">
          <Login />
        </Route>
      </Switch>
    </Router>
  )
}

export default App
